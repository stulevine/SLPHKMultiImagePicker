//
//  SLMultiImagePickerCellCollectionViewCell.swift
//  SLMultiImagePicker
//
//  Created by Stuart Levine on 3/18/18.
//  Copyright © 2018 Wildcatproductions. All rights reserved.
//

import UIKit

class SLPHKMultiImagePickerCell: UICollectionViewCell {

    @objc var imageSelected: Bool = false {
        didSet {
            overlayImageView.isHidden = !imageSelected
        }
    }
    var hasTypeOverlay: Bool = false
    @objc var imageView = UIImageView()
    var overlayImageView = UIImageView()
    var typeOverlayImageView = UIImageView()
    lazy var selectedImage: UIImage? = {
        let bundle = Bundle(identifier: "com.wildcatproductions.SLPHKMultiImagePicker")
        return UIImage(named: "selectedOverlay", in: bundle, compatibleWith: self.traitCollection)
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView.contentMode = .scaleAspectFill

        contentView.addSubview(imageView)
        contentView.addSubview(typeOverlayImageView)
        contentView.addSubview(overlayImageView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func resetSelected() {
        overlayImageView.isHidden = true
        imageSelected = false
    }

    func resetTypeOverlay() {
        hasTypeOverlay = false
        typeOverlayImageView.image = nil
    }

    @objc func showTypeOverlay(with image: UIImage?) {
        hasTypeOverlay = true
        typeOverlayImageView.image = image
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        contentMode = .center
        imageView.clipsToBounds = true
        let thumbNailSize = frame.width
        let imageRect = CGRect(x: 0, y: 0, width: thumbNailSize, height: thumbNailSize)
        imageView.frame = imageRect
        overlayImageView.frame = imageRect
        typeOverlayImageView.frame = imageRect
        overlayImageView.contentMode = .scaleAspectFit
        typeOverlayImageView.contentMode = .scaleAspectFit
        imageSelected = false
        hasTypeOverlay = false
        overlayImageView.image = selectedImage
    }

    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {

        guard let keyWindow = UIApplication.shared.keyWindow else {
            return layoutAttributes
        }

        var numberOfImages: CGFloat = 4.0
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            numberOfImages = 8.0
        }
        let width = (keyWindow.frame.width - (numberOfImages + 1)) / numberOfImages
        var frame = layoutAttributes.frame
        frame.size = CGSize(width: width, height: width)
        layoutAttributes.frame = frame
        return layoutAttributes
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        resetSelected()
        resetTypeOverlay()
        imageView.image = nil
    }
}
