//
//  SLPHKMultiImagePicker.h
//  SLPHKMultiImagePicker
//
//  Created by Stuart Levine on 5/2/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SLPHKMultiImagePicker.
FOUNDATION_EXPORT double SLPHKMultiImagePickerVersionNumber;

//! Project version string for SLPHKMultiImagePicker.
FOUNDATION_EXPORT const unsigned char SLPHKMultiImagePickerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SLPHKMultiImagePicker/PublicHeader.h>

