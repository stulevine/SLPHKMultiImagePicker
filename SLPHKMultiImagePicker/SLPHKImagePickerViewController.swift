//
//  SLPHKImagePickerViewController.swift
//  SLMultiImagePicker
//
//  Created by Stuart Levine on 3/19/18.
//  Copyright © 2018 Wildcatproductions. All rights reserved.
//

import UIKit
import Foundation
import Photos

protocol SLPHKImagePickerViewControllerDelegate : class {
    func willDismiss(_ viewController: SLPHKImagePickerViewController?, with selectedAssets: [PHAsset]?)
}

class SLPHKImagePickerViewController: UICollectionViewController {
    weak var imagePickerDelegate: SLPHKImagePickerViewControllerDelegate?

    var selectedAssets = [PHAsset]()
    var selectedVideoCount = 0
    lazy var thumbNailSize: CGFloat = {
        let size = (self.view.frame.width / 4.0) - 2.0*CGFloat(SLPHKPickerConstants.kSLInterimSpacing)
        return size
    }()
    var numberOfItemsPerRowForPortrait = 0
    var numberOfItemsPerRowForLandscape = 0
    var selectedImageCount = 0
    var forceTouchAvailable: Bool = false

    weak var presenter: SLPHKAlbumPickerViewController?

    private var fetchResults: PHFetchResult<PHAsset>?
    private let imageManager: PHCachingImageManager = PHCachingImageManager()
    private var albumTitle = ""
    private var footerText = ""

    private let flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(SLPHKPickerConstants.kSLLineSpacing, SLPHKPickerConstants.kSLLineSpacing, 0, SLPHKPickerConstants.kSLLineSpacing)
        layout.minimumInteritemSpacing = SLPHKPickerConstants.kSLInterimSpacing
        layout.minimumLineSpacing = SLPHKPickerConstants.kSLLineSpacing
        layout.footerReferenceSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: SLPHKPickerConstants.kSLFooterReferenceHeight)

        return layout
    }()

    init(with fetchResults: PHFetchResult<PHAsset>, title: String) {

        super.init(collectionViewLayout: flowLayout)

        collectionView?.backgroundColor = .white
        collectionView?.register(SLPHKMultiImagePickerCell.self, forCellWithReuseIdentifier: SLPHKPickerConstants.kSLMultiImagePickerCellReuseIdentifier)
        collectionView?.register(SLPHKFooterCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: SLPHKPickerConstants.kSLFooterReferenceReuseIdentifier)

        albumTitle = title

        flowLayout.estimatedItemSize = CGSize(width: thumbNailSize, height: thumbNailSize)

        self.fetchResults = fetchResults
        restorationIdentifier = "SLPHKImagePickerViewController"

        PHPhotoLibrary.shared().register(self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)

        title = albumTitle
        let count = fetchResults?.count ?? 0
        if count > SLPHKPickerConstants.kSLMinimumItemsBeforeScrollingToLastCell {
            self.collectionView?.scrollToItem(at: IndexPath(item: count - 1, section: 0), at: .bottom, animated: false)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        selectedAssets.removeAll()
        selectedVideoCount = 0

        navigationItem.title = albumTitle
        navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(finishedSelectingImages(_:)))

        if let collectionView = collectionView, view.traitCollection.forceTouchCapability != .unavailable  {
            registerForPreviewing(with: self, sourceView: collectionView)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    @objc func finishedSelectingImages(_ sender: UIBarButtonItem) {
        if sender.title == "Done" {
            imagePickerDelegate?.willDismiss(self, with: selectedAssets)
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchResults?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SLPHKPickerConstants.kSLMultiImagePickerCellReuseIdentifier, for: indexPath)

        guard let asset = fetchResults?[indexPath.item], let imageCell = cell as? SLPHKMultiImagePickerCell else { return cell }

        let iconSize: CGFloat = UIScreen.main.scale * thumbNailSize
        let options = PHImageRequestOptions()
        options.resizeMode = .fast
        options.deliveryMode = .opportunistic

        imageManager.requestImage(for: asset,
                                  targetSize: CGSize(width: iconSize, height: iconSize),
                                  contentMode: .aspectFit,
                                  options: options) { (image, info) in
                                    imageCell.imageView.image = image
        }

        switch asset.mediaType {
        case .video: imageCell.showTypeOverlay(with: videoImage)
        case .image:
            if (UInt8(asset.mediaSubtypes.rawValue) & UInt8(PHAssetMediaSubtype.photoPanorama.rawValue) > 0) {
                imageCell.showTypeOverlay(with: panoramaImage)
            }
        default: break
        }

        if selectedAssets.count > 0 {
            if selectedAssets.contains(asset) {
                imageCell.imageSelected = true
            }
        }

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? SLPHKMultiImagePickerCell,
            let asset = fetchResults?[indexPath.item] else { return }

        cell.imageSelected = !cell.imageSelected
        if cell.imageSelected {
            if asset.mediaType == .video {
                selectedVideoCount += 1
            }
            selectedAssets.append(asset)
        }
        else {
            if asset.mediaType == .video {
                selectedVideoCount -= 1
            }
            if let index = selectedAssets.index(of: asset) {
                selectedAssets.remove(at: index)
            }
        }

        navigationItem.rightBarButtonItem?.title = selectedAssets.count > 0 ? "Done" : "Cancel"
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let footerView = self.collectionView?.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: SLPHKPickerConstants.kSLFooterReferenceReuseIdentifier, for: indexPath)

        if kind == UICollectionElementKindSectionFooter, let footer = footerView as? SLPHKFooterCollectionReusableView {

            let assetCount = fetchResults?.count ?? 0

            if assetCount > SLPHKPickerConstants.kSLMinimumItemsBeforeScrollingToLastCell {
                let imageCount = fetchResults?.countOfAssets(with: .image) ?? 0
                let videoCount = fetchResults?.countOfAssets(with: .video) ?? 0

                var string = ""
                if imageCount > 0 {
                    string = "\(imageCount) Photos"
                }

                if videoCount > 0 {
                    if imageCount > 0 {
                        string += ", "
                    }
                    string = "\(string)\(videoCount) Videos"
                }
                footer.textLabel.text = string
            }
        }

        return footerView ?? UICollectionReusableView()
    }

    // MARK:- Utility

    lazy var videoImage: UIImage? = {
        let bundle = Bundle(identifier: "com.wildcatproductions.SLPHKMultiImagePicker")
        return UIImage(named: "videoOverlay", in: bundle, compatibleWith: view.traitCollection)
    }()

    lazy var panoramaImage: UIImage? = {
        let bundle = Bundle(identifier: "com.wildcatproductions.SLPHKMultiImagePicker")
        return UIImage(named: "panoOverlay", in: bundle, compatibleWith: view.traitCollection)
    }()

}

class SLPHKFooterCollectionReusableView: UICollectionReusableView {

    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        label.text = nil

        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        contentMode = .center
        addSubview(textLabel)
        textLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        textLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        textLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}

extension SLPHKImagePickerViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {

        if let indexPath = collectionView?.indexPathForItem(at: location), let cell = collectionView?.cellForItem(at: indexPath) as? SLPHKMultiImagePickerCell {
            previewingContext.sourceRect = cell.frame
            if let asset = fetchResults?[indexPath.item] {
                let detailVC = SLPHKDisplayViewController(nibName: nil, bundle: nil)
                detailVC.preferredContentSize = .zero
                detailVC.assetIsSelected = selectedAssets.contains(asset)
                detailVC.asset = asset
                detailVC.selectionBlock = { [weak self] in
                    guard let `self` = self, let collectionView = self.collectionView else { return }
                    DispatchQueue.main.async {
                        self.collectionView(collectionView, didSelectItemAt: indexPath)
                    }
                }
                return detailVC
            }
        }

        return nil
    }

    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: nil)
    }
}

extension SLPHKImagePickerViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        guard let fetchResults = fetchResults else { return }
        DispatchQueue.main.async { [unowned self] in
            let collectionChanges = changeInstance.changeDetails(for: fetchResults)
            if let changes = collectionChanges {
                self.fetchResults = changes.fetchResultAfterChanges
                if changes.hasIncrementalChanges || changes.hasMoves {
                    self.collectionView?.reloadData()
                }
                else {
                    self.collectionView?.performBatchUpdates({
                        if let removedIndexes = changes.removedIndexes {
                            self.collectionView?.deleteItems(at: removedIndexes.indexPathFromIndexes(in: 0))
                        }
                        if let insertedIndexes = changes.insertedIndexes {
                            self.collectionView?.insertItems(at: insertedIndexes.indexPathFromIndexes(in: 0))
                        }
                        if let changedIndexes = changes.changedIndexes {
                            self.collectionView?.reloadItems(at: changedIndexes.indexPathFromIndexes(in: 0))
                        }
                    }, completion: nil)
                }
            }
        }
    }
}

extension IndexSet {
    func indexPathFromIndexes(in section: Int) -> [IndexPath] {
        var indexPaths = [IndexPath]()

        for (index, _) in self.enumerated() {
            indexPaths.append(IndexPath(item: index, section: section))
        }
        return indexPaths
    }
}
