//
//  MapViewController.swift
//  SLMultiImagePicker
//
//  Created by Stuart Levine on 1/24/16.
//  Copyright © 2016 Wildcatproductions. All rights reserved.
//

import UIKit
import MapKit
import Foundation
import Photos

class SLPHKMapViewController: UIViewController {

    @IBOutlet var mapView: MKMapView!

    @objc var asset: PHAsset?
    @objc var assetName: String?
    
    lazy var imageManager: PHImageManager = {
        let imageManager: PHImageManager = PHImageManager.default()
        
        return imageManager
    }()
    
    lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        return locationManager
    }()
    
    var currentLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()

        updateMapView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        locationManager.stopUpdatingLocation()
    }
    
    func updateMapView() {
        if let coordinate = asset?.location?.coordinate {
            let annotation: PhotoAnnotation = PhotoAnnotation(coordinate: coordinate, title: assetName, subtitle: "")
            
            let spanRadius: CLLocationDegrees = 0.015
            let span: MKCoordinateSpan = MKCoordinateSpanMake(spanRadius, spanRadius)
            let region: MKCoordinateRegion = MKCoordinateRegionMake(annotation.coordinate, span)
            
            mapView.setRegion(region, animated: true)
            mapView.addAnnotation(annotation);
            
            if asset != nil {
                if let annotationView = mapView(mapView, viewFor: annotation) {
                    mapView(mapView, didSelect: annotationView)
                }
            }
        }
    }

    @available(iOS 9.0, *)
    override var previewActionItems : [UIPreviewActionItem] {
        let selectAction: UIPreviewAction = UIPreviewAction(title: "Done", style: .default) { (action, viewController) -> Void in
            print("Done here")
        }
        
        return [selectAction]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: - MKMapViewDelegate

extension SLPHKMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        guard let annotationView: MKAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "photoAnnotation") else {
            let newAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "photoAnnotation")
            newAnnotationView.pinTintColor = .red
            newAnnotationView.canShowCallout = true
            newAnnotationView.leftCalloutAccessoryView = UIImageView(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: 30.0, height: 30.0)))

            return newAnnotationView
        }
        
        annotationView.canShowCallout = true
        annotationView.leftCalloutAccessoryView = UIImageView(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: 30.0, height: 30.0)))

        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if (view.annotation?.isKind(of: PhotoAnnotation.self) == true) {
            let imageSize = 30.0 * UIScreen.main.scale
            if view.leftCalloutAccessoryView == nil {
                let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: 30.0, height: 30.0)))
                imageView.contentMode = .scaleAspectFill
                view.leftCalloutAccessoryView = imageView
            }
            
            let options = PHImageRequestOptions()
            options.resizeMode = PHImageRequestOptionsResizeMode.fast
            options.deliveryMode = PHImageRequestOptionsDeliveryMode.fastFormat
 
            if let asset = self.asset {
                imageManager.requestImage(for: asset, targetSize: CGSize(width: imageSize, height: imageSize), contentMode: .aspectFill, options: options, resultHandler: { (image, info) -> Void in
                    if image != nil {
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let imageView: UIImageView = view.leftCalloutAccessoryView as? UIImageView,  let annotation = view.annotation {
                                
                                imageView.image = image
                                mapView.selectAnnotation(annotation, animated: false)
                            }
                        })
                    }
                })
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension SLPHKMapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last
    }

}

// MARK: - PhotoAnnotation

class PhotoAnnotation: NSObject, MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }

}
