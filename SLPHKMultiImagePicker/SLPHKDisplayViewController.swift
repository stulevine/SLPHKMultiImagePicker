//
//  DisplayViewController.swift
//  Netbility
//
//  Created by Stuart Levine on 10/9/15.
//  Copyright © 2015 Wildcatproductions. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import Photos

class SLPHKDisplayViewController: UIViewController {

    typealias ImageCompletionBlock = (_ image: UIImage) -> ()
    typealias SelectionBlock = () -> ()

    var imageView: UIImageView?
    var imageToDisplay: UIImage?
    var shadowView: UIView?
    var buttonTitle = "Select"

    @objc weak var asset: PHAsset?
    @objc var assetIsSelected: Bool = false {
        didSet {
            switch assetIsSelected {
            case true: self.buttonTitle = "Deselect"
            case false: self.buttonTitle = "Select"
            }
        }
    }
    
    @objc var imageCompletionBlock: ImageCompletionBlock?
    @objc var selectionBlock: SelectionBlock?
    
    let kShadowRadius: CGFloat = 5.0
    let kCornerRadius: CGFloat = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear

        self.imageCompletionBlock = { [unowned self] (image: UIImage) in
            self.setupImageViewWithImage(image)
        }

        let imageManager: PHCachingImageManager = PHCachingImageManager()

        if let mediaType = asset?.mediaType {
            switch  mediaType {
            case .image:
                if imageToDisplay != nil {
                    self.setupImageViewWithImage(imageToDisplay!)
                }
                else {
                    if let imageAsset = asset {
                        let requestOptions = PHImageRequestOptions()
                        requestOptions.resizeMode = .fast
                        requestOptions.deliveryMode = .fastFormat
                        imageManager.requestImageData(for: imageAsset, options: requestOptions, resultHandler: { (data, dataUTI, orientation, info) -> Void in
                            
                            if let image: UIImage = UIImage(data: data!), data != nil {
                                DispatchQueue.main.async(execute: {
                                    self.setupImageViewWithImage(image)
                                })
                            }
                        })
                    }
                }
            case .video:
                if let videoAsset = asset {
                    let requestOptions = PHVideoRequestOptions()
                    requestOptions.deliveryMode = .fastFormat
                    imageManager.requestAVAsset(forVideo: videoAsset, options: requestOptions, resultHandler: { (avAsset, audioMix, info) -> Void in
                        if let urlAsset = avAsset as? AVURLAsset {
                            let pixelWidth = CGFloat(videoAsset.pixelWidth)
                            let pixelHeight = CGFloat(videoAsset.pixelHeight)
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.setupVideoPlayerWith(urlAsset.url, videoSize: CGSize(width: pixelWidth, height: pixelHeight), asset: avAsset!)
                            })
                        }
                    })
                }
            default: break
            }
        }
        
        
    }
    
    func setupVideoPlayerWith(_ videoURL: URL, videoSize: CGSize, asset: AVAsset) {
        let viewSize = view.frame.size
        
        let imageSize = videoSize
        let scaleRatio: CGFloat = min(viewSize.width / imageSize.width, viewSize.height / imageSize.height)
        let h = imageSize.height * scaleRatio
        let w = imageSize.width * scaleRatio
        
        let shadowView: UIView = UIView(frame: CGRect.zero)
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.layer.cornerRadius = kCornerRadius
        shadowView.layer.shadowColor = UIColor ( red: 0.2999, green: 0.2999, blue: 0.2999, alpha: 1.0 ).cgColor
        shadowView.layer.shadowOffset = CGSize(width: kShadowRadius, height: kShadowRadius)
        shadowView.layer.shadowOpacity = 0.8
        shadowView.layer.shadowRadius = kShadowRadius
        shadowView.layer.masksToBounds = true
        self.shadowView = shadowView;
        self.view.addSubview(shadowView)

        shadowView.widthAnchor.constraint(equalToConstant: w+30.0).isActive = true
        shadowView.heightAnchor.constraint(equalToConstant: h+30.0).isActive = true
        shadowView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        let playerItem = AVPlayerItem(asset: asset)
        let player = AVPlayer(playerItem: playerItem)
        let playerViewController = AVPlayerViewController()
        addChildViewController(playerViewController)

        playerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        playerViewController.player = player
        playerViewController.showsPlaybackControls = false

        playerViewController.view.layer.cornerRadius = kCornerRadius
        playerViewController.view.layer.masksToBounds = true
        playerViewController.view.contentMode = .scaleAspectFit

        shadowView.addSubview(playerViewController.view)

        playerViewController.view.widthAnchor.constraint(equalToConstant: w).isActive = true
        playerViewController.view.heightAnchor.constraint(equalToConstant: h).isActive = true
        playerViewController.view.centerXAnchor.constraint(equalTo: shadowView.centerXAnchor).isActive = true
        playerViewController.view.centerYAnchor.constraint(equalTo: shadowView.centerYAnchor).isActive = true

        player.play()
    }
    
    func setupImageViewWithImage(_ imageToDisplay: UIImage) {
        let viewSize = view.frame.size        
        let imageSize: CGSize = imageToDisplay.size
        let scaleRatio: CGFloat = min(viewSize.width / imageSize.width, viewSize.height / imageSize.height)
        let h = imageSize.height * scaleRatio
        let w = imageSize.width * scaleRatio

        let shadowView: UIView = UIView(frame: CGRect.zero)
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.layer.cornerRadius = kCornerRadius
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: kShadowRadius, height: kShadowRadius)
        shadowView.layer.shadowOpacity = 0.8
        shadowView.layer.shadowRadius = kShadowRadius
        shadowView.layer.masksToBounds = true
        self.view.addSubview(shadowView)

        shadowView.widthAnchor.constraint(equalToConstant: w+30.0).isActive = true
        shadowView.heightAnchor.constraint(equalToConstant: h+30.0).isActive = true
        shadowView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        let imageView: UIImageView = UIImageView(image: imageToDisplay)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = kCornerRadius
        shadowView.addSubview(imageView)

        imageView.widthAnchor.constraint(equalToConstant: w).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: h).isActive = true
        imageView.centerYAnchor.constraint(equalTo: shadowView.centerYAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: shadowView.centerXAnchor).isActive = true

        self.shadowView = shadowView;
        self.imageView = imageView
    }
    
    @available(iOS 9.0, *)
    override var previewActionItems : [UIPreviewActionItem] {
        let selectAction: UIPreviewAction = UIPreviewAction(title: self.buttonTitle, style: .default) { [unowned self] (action, viewController) -> Void in
            if let selectionBlock: SelectionBlock = self.selectionBlock {
                selectionBlock();
            }
        }
        
        return [selectAction]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
