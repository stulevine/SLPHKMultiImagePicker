//
//  SLPHKAlbumPickerViewController.swift
//  SLMultiImagePicker
//
//  Created by Stuart Levine on 3/18/18.
//  Copyright © 2018 Wildcatproductions. All rights reserved.
//

import UIKit
import Foundation
import Photos

struct SLPHKPickerConstants {
    static let kSLInterimSpacing: CGFloat = 1.0
    static let kSLLineSpacing = kSLInterimSpacing
    static let kSLNumberOfItemsPerRowForPortrait = 4
    static let kSLNumberOfItemsPerRowForLandscape = 7
    static let kSLAdditionItemsForWideScreens = 2
    static let kSLFooterReferenceHeight: CGFloat = 40.0
    static let kSLMinimumItemsBeforeScrollingToLastCell = 24
    static let kSLMultiImagePickerCellReuseIdentifier = "kSLMultiImagePickerCellReuseIdentifier"
    static let kSLFooterReferenceReuseIdentifier = "kSLFooderReferenceReuseIdentifier"
}

let kSLAlbumCellHeight: CGFloat = 86.0
let kSLPHKAlbumTableViewCell = "SLPHKAlbumTableViewCell"

@objc public protocol SLPHKAlbumPickerViewControllerDelegate : class {
    func didDismissPHKAlbumPicker(_ viewController: SLPHKAlbumPickerViewController?, with selectedAssets: [Any]?)
}

public class SLPHKAlbumPickerViewController: UITableViewController {
    @objc public weak var presenterDelegate: SLPHKAlbumPickerViewControllerDelegate?

    static func viewController(withRestorationIdentifierPath identifierComponents: [Any], coder: NSCoder) -> UIViewController? {
        return SLPHKAlbumPickerViewController()
    }

    let imageManager = PHCachingImageManager()
    var collectionsFetchResults: [PHAssetCollection]?
    var selectedIndexPath: IndexPath?
    var videoCount = 0
    var imageCount = 0

    @objc public init() {
        super.init(style: .plain)

        tableView.register(SLPHKAlbumPickerCell.self, forCellReuseIdentifier: kSLPHKAlbumTableViewCell)
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)

        title = "Photos"
        restorationIdentifier = "SLPHKAlbumPickerViewController"

        PHPhotoLibrary.shared().register(self)

        self.fetchPhotoAlbums()
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        title = "Photos"
        restorationIdentifier = "SLPHKAlbumPickerViewController"
        PHPhotoLibrary.shared().register(self)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().barTintColor = .white

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAlbumPicker(_:)))
    }

    func fetchPhotoAlbums() {
        let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "localizedTitle", ascending: true)]
        let topLevelUserCollections = PHCollectionList.fetchTopLevelUserCollections(with: fetchOptions)
        var results = [PHAssetCollection]()
        smartAlbums.enumerateObjects { (asset, index, stop) in
            let assetsFetchResults = PHAsset.fetchAssets(in: asset, options: nil)
            if (assetsFetchResults.count > 0) {
                results.insert(asset, at: 0)
            }
        }
        topLevelUserCollections.enumerateObjects { (asset, index, stop) in
            if let assetCollection = asset as? PHAssetCollection {
                let assetsFetchResults = PHAsset.fetchAssets(in: assetCollection, options: nil)
                if (assetsFetchResults.count > 0) {
                    results.append(assetCollection)
                }
            }
        }
        collectionsFetchResults = results
    }

    @objc func cancelAlbumPicker(_ sender: UIBarButtonItem) {
        dismiss(animated: true) { [unowned self] in
            self.presenterDelegate?.didDismissPHKAlbumPicker(self, with: [])
        }
    }

    func metaData(from imageData: NSData) -> [AnyHashable : Any]? {
        let cfData = NSData(bytes: imageData.bytes, length: MemoryLayout<sockaddr_in>.size)
        if let imageSource = CGImageSourceCreateWithData(cfData, nil) {
            let options = [kCGImageSourceShouldCache: false] as CFDictionary
            if let imageProperties = CGImageSourceCopyProperties(imageSource, options) {
                return imageProperties as? [String: AnyObject]
            }
        }

        return nil
    }

    // TableView DataSource Delegate
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collectionsFetchResults?.count ?? 0
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let newCell = tableView.dequeueReusableCell(withIdentifier: kSLPHKAlbumTableViewCell, for: indexPath)
        guard let cell = newCell as? SLPHKAlbumPickerCell, let collectionsFetchResults = collectionsFetchResults  else { return newCell }

        //cell.contentView.translatesAutoresizingMaskIntoConstraints = false
        cell.accessoryType = .disclosureIndicator
        let collection = collectionsFetchResults[indexPath.row]

        let fetchResults = PHAsset.fetchAssets(in: collection, options: nil)
        let imageCount = fetchResults.countOfAssets(with: .image)
        let videoCount = fetchResults.countOfAssets(with: .video)
        print("\(imageCount), \(videoCount)")
        if let first = fetchResults.firstObject, fetchResults.count > 0 {
            let iconSize = UIScreen.main.scale * 70.0
            imageManager.requestImage(for: first, targetSize: CGSize(width: iconSize, height: iconSize), contentMode: .aspectFit, options: nil, resultHandler: { (image, indo) in
                cell.iconImageView.image = image
            })
        }
        cell.titleLabel.text = collection.localizedTitle

        var detailText = ""
        if imageCount > 0 {
            detailText = "\(detailText)\(imageCount) Photos"
        }
        if videoCount > 0 {
            if detailText.count > 0 {
                detailText += ", "
            }
            detailText = "\(detailText)\(videoCount) Videos"
        }
        cell.subtitleLabel.text = detailText

        return cell
    }

    // MARK: - Table view data source

    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kSLAlbumCellHeight
    }

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let asset = collectionsFetchResults?[indexPath.row] {
            let fetchResults = PHAsset.fetchAssets(in: asset, options: nil)
            selectedIndexPath = indexPath
            let cell = tableView.cellForRow(at: indexPath) as? SLPHKAlbumPickerCell
            let imagePickerVC = SLPHKImagePickerViewController(with: fetchResults, title: cell?.textLabel?.text ?? "")
            imagePickerVC.imagePickerDelegate = self
            navigationController?.pushViewController(imagePickerVC, animated: true)
        }
    }

}

extension SLPHKAlbumPickerViewController: PHPhotoLibraryChangeObserver {
    public func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async { [unowned self] in
            self.fetchPhotoAlbums()
            self.tableView?.reloadData()
        }
    }
}

extension UIImage {
    func imageByCropping(_ image: UIImage?, to rect: CGRect) -> UIImage? {
        guard let cgImage = image?.cgImage, let croppedCGImage = cgImage.cropping(to: rect) else { return nil }

        return UIImage(cgImage: croppedCGImage)
    }
}

extension SLPHKAlbumPickerViewController: SLPHKImagePickerViewControllerDelegate {
    func willDismiss(_ viewController: SLPHKImagePickerViewController?, with selectedAssets: [PHAsset]?) {
        self.dismiss(animated: true) { [unowned self] in
            self.presenterDelegate?.didDismissPHKAlbumPicker(self, with: selectedAssets)
        }
    }

}
